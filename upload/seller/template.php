<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Template page</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="./lib/chosen/docsupport/prism.css">
        <link rel="stylesheet" href="./lib/chosen/chosen.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js" type="text/javascript"></script> 
        <script src="./lib/chosen/chosen.jquery.js" type="text/javascript"></script>
        <script src="./lib/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

    </head>
    <body>
	<div class="row">
        <div class="container col-lg-10 col-lg-offset-1 divBdr">
            <div class="row divBdr">               
                <div class="col-lg-12 divBdr">
                    <?php
                    // navBarMainLogedIn();
                    ?>
                </div>
            </div>
        </div>
	</div>
		
    <div class="container">
        <form method="post" action="">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">Add/Edit Customer</div>
                    <div class="panel-body">
                        <div  class="col-xs-12 alert-info"> <br />								

                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">Customer Name</label>
                                <div class="col-xs-9">
                                    <select class="chosen-select form-control " data-placeholder="Enter Customer Name..." id="customerNameList" name="brand">
                                        <option value=""></option>
                                        <option value="">test</option>
                                            <?php
                                                /*
                                                while ($row = $customerList->fetch_assoc()){
                                                unset($customerID, $customerFirstName, $customerLasttName);
                                                $customerID = $row['customerId'];
                                                $customerFirstName = $row['firstName'];
                                                $customerLasttName = $row['lastName'];
                                                echo '<option value="'.$customerID.'">'.$customerFirstName.' '.$customerLasttName.'</option>';                                                    
                                                }
                                                */
                                            ?>																				
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div  class="col-xs-12 alert-info">	<br />							
                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                                                <input type="hidden" id="customerIdHidden" name="customerIdHidden" value="0">
                                <label class="col-xs-3" for="brand">First Name</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerFirstName" placeholder="First Name" name="customerFirstName">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">Last Name</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerLastName" placeholder="Last Name" name="customerLastName">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">Mobile</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerMobile" placeholder="Mobile" name="customerMobile">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">Tel.</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerTel" placeholder="Telephone" name="customerTel">
                            </div>

                        </div>
                        
                        
                    </div>
                </div>
                
                <div class="panel panel-primary">						
                    <div class="panel-body">
                        <div  class="col-xs-12 alert-info">
                        <h5>Customer Address</h5>
                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">Number</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerAddNo" placeholder="Street Number" name="brand">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">Street</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerAddStreet" placeholder="Street Name" name="brand">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">Suburb</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerAddSuburb" placeholder="Suburb" name="brand">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">State</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerAddState" placeholder="State" name="brand">
                            </div>
                                                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">Post Code</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerAddPostCode" placeholder="Post Code" name="brand">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 text-right">
                                <label class="col-xs-3" for="brand">Country</label>
                                <input class="col-xs-9" type="text" class="form-control" id="customerAddCountry" placeholder="Country" name="brand">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">						
                    <div class="panel-body">
                        <div  class="col-xs-12 alert-info">	<br />
                            <div class="form-group col-xs-12 text-right">
                                                                    <button type="button" class="btn btn-primary" id="addCustomer">Add Customer</button>
                                <button type="button" class="btn btn-primary" id="editCustomer">Edit Customer</button>
                                                                    <button type="button" class="btn btn-primary" id="deleteCustomer">Delete Customer</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
	
	
	
  <script src="./lib/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script>
  
	</body>
</html>
